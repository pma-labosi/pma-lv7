package com.example.layoutparty

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import android.widget.ToggleButton

class MainActivity : AppCompatActivity() {
    public lateinit var btnPrint: Button
    public lateinit var editText: EditText
    public lateinit var rbDa: RadioButton
    public lateinit var rbNe: RadioButton
    public lateinit var cb1: CheckBox
    public lateinit var cb2: CheckBox
    public lateinit var cb3: CheckBox
    public lateinit var cb4: CheckBox
    public lateinit var toggle: ToggleButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPrint = findViewById(R.id.print)
        editText = findViewById(R.id.editText)
        rbDa = findViewById(R.id.rbDa)
        rbNe = findViewById(R.id.rbNe)
        cb1 = findViewById(R.id.cb1)
        cb2 = findViewById(R.id.cb2)
        cb3 = findViewById(R.id.cb3)
        cb4 = findViewById(R.id.cb4)
        toggle = findViewById(R.id.toggle)

        btnPrint.setOnClickListener {
            var message: String = "editText: " + editText.text.toString()
            message += "\nrbDa: " + rbDa.isChecked.toString()
            message += "\nrbNe: " + rbNe.isChecked.toString()
            message += "\ncb1: " + cb1.isChecked.toString()
            message += "\ncb2: " + cb2.isChecked.toString()
            message += "\ncb3: " + cb3.isChecked.toString()
            message += "\ncb4: " + cb4.isChecked.toString()
            message += "\ntoggle: " + toggle.isChecked.toString()
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }

        rbDa.setOnClickListener {
            Toast.makeText(this, "Da", Toast.LENGTH_SHORT).show()
        }

        rbNe.setOnClickListener {
            Toast.makeText(this, "Ne", Toast.LENGTH_SHORT).show()
        }

        cb1.setOnClickListener {
            Toast.makeText(this, "1", Toast.LENGTH_SHORT).show()
        }

        cb2.setOnClickListener {
            Toast.makeText(this, "2", Toast.LENGTH_SHORT).show()
        }

        cb3.setOnClickListener {
            Toast.makeText(this, "3", Toast.LENGTH_SHORT).show()
        }

        cb4.setOnClickListener {
            Toast.makeText(this, "4", Toast.LENGTH_SHORT).show()
        }

        toggle.setOnClickListener {
            Toast.makeText(this, toggle.text.toString(), Toast.LENGTH_SHORT).show()
        }
    }
}